TARGET = ../../web/br2017.mini.debconf.org/

export:
	cp -alf website/* $(TARGET)

website/programacao.shtml: website/programacao.rst template.txt
	rst2html --no-doc-title --initial-header-level=2 --template=template.txt $< $@

reload:
	$(RM) website/programacao.shtml
	$(MAKE)
