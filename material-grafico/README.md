# MiniDebConf Curitiba 2017

Material gráfico criado para a MiniDebConf Curitiba 2017 realizada nos dias 17,
18 e 19 de março na UTFPR.

Autor: Paulo Henrique de Lima Santana.

Arquivos SVG e PNG em vários tamanhos.

Site: http://br2017.mini.debconf.org

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/) 
