1i [background.png][stage-color=white][text-color=#2e3436][shading-opacity=0.0][font=Prociono TT 60px]
s#\[title\]#[title.png][text-color=white]#
s#\[image\]#[bottom][shading-opacity=0.8][text-color=white]#
s#\[hugetext\]#[font=Prociono TT 350px]#

s@<code>@<span font="Monospace" color="#2a3436">@g
s@</code>@</span>@g
s/@\([^@]*\)@/<span font="Monospace" color="#2a3436">\1<\/span>/g

s@^\(  \)*\*@\1∙@
s@^\(\W\) @<span font="Monospace">\1 </span>@
s@^  \(\W\) @<span font="Monospace">  \1 </span>@
s@^  @<span font="Monospace">  </span>@
s@^    @<span font="Monospace">    </span>@

s@<b>@<span color="#a40000"><b>@g
s@</b>@</b></span>@g

s@<strong>@<b>@g
s@</strong>@</b>@g
