Programação
-----------

A programação da MiniDebConf Curitiba 2017 é composta por dois tipos principais
de atividades:

* **Palestras**: apresentações sobre uso ou desenvolvimento do Debian.
  Acontecerão no **miniauditório**.
* **Oficinas**: atividades práticas voltadas a novos e atuais usuários do
  Debian.

Ao longo dos três dias acontecerão ainda outras atividades permanentes como:

* Instalação de Debian nos notebooks/computadores daqueles que precisarem.
* Criação de chaves GPG.
* `BSP - Bug Squashing Party`_

**Para participar das oficinas traga o seu próprio notebook com Debian
instalado. Não haverá computadores disponíveis para os participantes.** 

Sexta-feira, 17/03
~~~~~~~~~~~~~~~~~~

+---------+---------------------------------+
| Horário | Palestras (miniauditório)       |
+=========+=================================+
| 14:00   | Credenciamento                  |
|         |                                 |
|         | *(também pode ser feito em      |
|         | qualquer outro momento)*        |
+---------+---------------------------------+
| 15:00 - | MiniDebconf Brasil 2017:        |
| 15:50   | abertura, boas-vindas, código   |
|         | de conduta, visão geral da      |
|         | programação e orientações gerais|
|         |                                 |
|         | *Organização*                   |
|         |                                 |
|         | `[video]`__ `[slides]`__        |
+---------+---------------------------------+
| 16:00 - | `Introdução ao Debian`_         |
| 16:50   |                                 |
|         | Samuel Henrique                 |
|         |                                 |
|         | `[video]`__                     |
+---------+---------------------------------+
| 17:00 - | `Debian: um universo em         |
| 17:50   | construção`_                    |
|         |                                 |
|         | Giovani Ferreira                |
|         |                                 |
|         | `[video]`__ `[slides]`__        |
+---------+---------------------------------+
| 18:00 - | `Conheça o trabalho da equipe   |
| 18:50   | de tradução do Debian`_         |
|         |                                 |
|         | Daniel Lenharo de Souza         |
|         |                                 |
|         | `[video]`__ `[slides]`__        |
+---------+---------------------------------+

__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/00_Abertura.webm
__ abertura/minidc2017-abertura.pdf
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/01_Introdu%c3%a7%c3%a3o_ao_Debian.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/02_Debian_um_universo_em_constru%c3%a7%c3%a3o.webm
__ slides/universo-em-construcao.pdf
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/03_Conhe%c3%a7a_o_trabalho_da_equipe_de_tradu%c3%a7%c3%a3o_do_Debian.webm
__ slides/debian-l10n.odp


Sábado, 18/03
~~~~~~~~~~~~~

+---------+--------------------------------+--------------------------------+--------------------------------+
| Horário | Palestras (miniauditório)      | Oficinas 1 (B-106)             | Oficinas 2 (B-109)             |
+=========+================================+================================+================================+
| 10:00 - | Orientações e avisos do dia    |                                |                                |
| 10:05   |                                |                                |                                |
|         | Organização                    |                                |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 10:10 - | `O sistema operacional         | `Debian on gadgets`_           | `Minicurso: empacotamento de   |
| 10:50   | universal e a educação`_       |                                | software no Debian`_           |
|         |                                | Thadeu Cascardo                |                                |
|         | Antônio C C Marques            |                                | Eriberto Mota                  |
|         |                                | `[slides]`__                   |                                |
|         | `[video]`__                    |                                |                                |
+---------+--------------------------------+                                |                                |
| 11:00 - | `Gerenciamento de Configurações|                                |                                |
| 11:50   | no Debian com Puppet`_         |                                |                                |
|         |                                |                                |                                |
|         | Gustavo Soares de Lima         |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__                    |                                |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 12:00 - | **intervalo para almoço**                                       |                                |
| 14:00   |                                                                 |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 14:00 - | `Vivendo perigosamente!?       | `Debian on gadgets`_ (cont)    | `Minicurso: empacotamento de   |
| 14:50   | Usando Debian unstable no      |                                | software no Debian`_ (cont)    |
|         | dia-a-dia`_                    | Thadeu Cascardo                |                                |
|         |                                |                                |                                |
|         | Antonio Terceiro               |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__ `[slides]`__       |                                |                                |
+---------+--------------------------------+                                | Eriberto Mota                  |
| 15:00 - | `Debate: Eventos Debian        |                                |                                |
| 15:50   | Brasil em 2017`_               |                                |                                |
|         |                                |                                |                                |
|         | Giovani Ferreira (org.) e      |                                |                                |
|         | demais interessados            |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__                    |                                |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 16:00 - | **Foto oficial**                                                                                 |
|         |                                                                                                  |
|         | Todos os participantes presentes - local exato será avisado durante o dia                        |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 16:15 - | **intervalo para coffee-break**                                                                  |
| 17:00   |                                                                                                  |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 17:00 - | `Painel: Debian Teams`_        | `Mantendo o seu Debian SID     | `Minicurso: empacotamento de   |
| 18:20   |                                | saudável e sem coisas          | software no Debian`_ (cont)    |
|         | Antonio Terceiro,              | desnecessárias`_               |                                |
|         | Daniel Lenharo,                |                                |                                |
|         | Eriberto Mota,                 | Paulo Kretcheu                 | Eriberto Mota                  |
|         | Giovani Ferreira,              |                                |                                |
|         | Lucas Kanashiro,               |                                |                                |
|         | Paulo Santana                  |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__                    |                                |                                |
+---------+--------------------------------+                                |                                |
| 18:30 - | `Lightning talks`_             |                                |                                |
| 19:00   |                                |                                |                                |
|         | *Diversos palestrantes*        |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__                    |                                |                                |
|         |                                |                                |                                |
|         |                                |                                |                                |
|         |                                |                                |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+

__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/04_O_sistema_operacional_universal_e_a_educa%c3%a7%c3%a3o.webm
__ slides/oficina_debian_gadgets.odp
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/05_Gerenciamento_de_Configura%c3%a7%c3%b5es_no_Debian_com_Puppet.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/06_Vivendo_perigosamente_Usando_Debian_unstable_no_dia-a-dia.webm
__ slides/unstable.pdf
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/07_Debate_Eventos_Debian_Brasil_em_2017.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/08_Painel_Debian_Teams.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/09_Lightning_Talks.webm

Domingo, 19/03
~~~~~~~~~~~~~~


+---------+--------------------------------+--------------------------------+--------------------------------+
| Horário | Palestras (miniauditório)      | Oficinas 1 (B-106)             | Oficinas 2 (B-109)             |
+=========+================================+================================+================================+
| 10:00   | Orientações e avisos do dia    |                                |                                |
|         |                                |                                |                                |
|         | Organização                    |                                |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 10:10 - | `Bootstrapping de novas        | `Minicurso: mulheres no Debian | `Dos primeiros passos          |
| 10:50   | arquiteturas no Debian`_       | - empacotamento de software    | no Terminal Linux à            |
|         |                                | e inclusão feminina`_          | Programação Shell Script`_     |
|         | Breno Henrique Leitão,         |                                |                                |
|         | Fernando Seiti Furusato        | Eriberto Mota                  | Gustavo Soares de Lima         |
|         |                                |                                |                                |
|         | `[video]`__                    |                                |                                |
+---------+--------------------------------+                                |                                |
| 11:00 - | `Teste automatizado de         |                                |                                |
| 11:50   | pacotes Debian`_               |                                |                                |
|         |                                |                                |                                |
|         | Antonio Terceiro               |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__ `[slides]`__       |                                |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 12:00 - | **intervalo para almoço**                                                                        |
| 14:00 - |                                                                                                  |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 14:00 - | `ppc64el: conheça a            | `Inteligência artificial com   | `Dos primeiros passos          |
| 14:25   | arquitetura mais rápida        | Debian - configuração e        | no Terminal Linux à            |
|         | do Debian hoje`_               | exemplos práticos`_            | Programação Shell Script`_     |
|         |                                |                                | (cont.)                        |
|         | Breno Henrique Leitão          | Wellton Costa de Oliveira      |                                |
|         |                                |                                | Gustavo Soares de Lima         |
|         | `[video]`__                    |                                |                                |
+---------+--------------------------------+                                |                                |
| 14:30 - | `Obtendo sua própria           |                                |                                |
| 14:55   | máquina Power`_                |                                |                                |
|         |                                |                                |                                |
|         | Fernando Seiti Furusato        |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__ `[slides]`__       |                                |                                |
+---------+--------------------------------+                                |                                |
| 15:00 - | `Ultimate Debian Database      |                                |                                |
| 15:25   | (UDD) e debian.net`_           |                                |                                |
|         |                                |                                |                                |
|         | Eriberto Mota                  |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__                    |                                |                                |
|         |                                |                                |                                |
|         | Bonus: Mulheres do meu Brasil, |                                |                                |
|         | cadê vocês? `[video]`__        |                                |                                |
+---------+--------------------------------+                                |                                |
| 15:30 - | `Se prendendo com schroot`_    |                                |                                |
| 15:55   |                                |                                |                                |
|         | Carlos Donizete Froes          |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__ `[slides]`__       |                                |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 16:00 - | **intervalo para coffee-break**                                                                  |
| 17:00   |                                                                                                  |
+---------+--------------------------------+--------------------------------+--------------------------------+
| 17:00 - | `Painel: perguntas e           |                                |                                |
| 17:50   | respostas sobre o              |                                |                                |
|         | desenvolvimento do             |                                |                                |
|         | Debian`_                       |                                |                                |
|         |                                |                                |                                |
|         | Antonio Terceiro,              |                                |                                |
|         | Eriberto Mota,                 |                                |                                |
|         | Giovani Ferreira,              |                                |                                |
|         | Lucas Kanashiro                |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__                    |                                |                                |
+---------+--------------------------------+                                |                                |
| 18:00   | Encerramento                   |                                |                                |
|         |                                |                                |                                |
|         | *Organização*                  |                                |                                |
|         |                                |                                |                                |
|         | `[video]`__ `[slides]`__       |                                |                                |
+---------+--------------------------------+--------------------------------+--------------------------------+

__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/10_Bootstrapping_de_novas_arquiteturas_no_Debian.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/11_Teste_automatizado_de_pacotes_Debian.webm
__ slides/functional-testing-of-debian-packages.pdf
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/12_ppc64el_conhe%c3%a7a_a_arquitetura_mais_r%c3%a1pida_do_Debian_hoje.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/13_Obtendo_sua_pr%c3%b3pria_m%c3%a1quina_Power.webm
__ slides/MiniCloud_pt.pdf
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/14_Ultimate_Debian_Database_UDD_e_debian.net.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/15_Mulheres_do_meu_Brasil,_cad%c3%aa_voc%c3%aas.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/16_Se_prendendo_com_schroot.webm
__ slides/apresentacao-schroot.pdf
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/17_Painel_perguntas_e_respostas_sobre_o_desenvolvimento_do_Debian.webm
__ http://meetings-archive.debian.net/pub/debian-meetings/2017/mini-debconf-curitiba/18_Encerramento.webm
__ slides/encerramento.odp



.. ~~~~~~~~~~~~~~~~~~~~ links para descrições das palestras ~~~~~~~~~~~~~~~~~~~

.. _`O sistema operacional universal e a educação`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#antonio-educacao
.. _`Minicurso: empacotamento de software no Debian`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#eriberto-minicurso
.. _`Lightning talks`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#lightning-talks
.. _`Mantendo o seu Debian SID saudável e sem coisas desnecessárias`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#kretcheu-sid
.. _`Painel: Debian Teams`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#painel-teams
.. _`Bootstrapping de novas arquiteturas no debian`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#leitao-bootstrap
.. _`Minicurso: mulheres no Debian - empacotamento de software e inclusão feminina`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#Minicurso:_mulheres_no_Debian_-_empacotamento_de_software_e_inclus.2BAOM-o_feminina
.. _`Teste automatizado de pacotes debian`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#terceiro-teste
.. _`ppc64el: conheça a arquitetura mais rápida do debian hoje`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#leitao-ppc64el
.. _`Inteligência artificial com debian - configuração e exemplos práticos`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#wellton-ia
.. _`Obtendo sua própria máquina power`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#ferseiti-power
.. _`Painel: perguntas e respostas sobre o desenvolvimento do debian`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#painel-perguntas
.. _`Ultimate debian database (udd) e debian.net`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#eriberto-udd-debian.net
.. _`Se prendendo com schroot`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#coringao-schroot
.. _`Debate: eventos debian brasil em 2017`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#debate-eventos
.. _`Vivendo perigosamente!? usando debian unstable no dia-a-dia`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#terceiro-sid
.. _`Debian on gadgets`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#cascardo-gadgets
.. _`Conheça o trabalho da equipe de tradução do debian`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#lenharo-l10n
.. _`BSP - Bug Squashing Party`: https://wiki.debian.org/BSP
.. _`introdução ao debian`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#samuel-debian101
.. _`debian: um universo em construção`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#giovani-universo
.. _`gerenciamento de configurações no debian com puppet`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#gustavo-puppet
.. _`dos primeiros passos no terminal linux à programação shell script`: https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP#gustavo-shell
